<?php declare (strict_types = 1);

function printContent(array $content): void
{
    echo "<h1>";
    echo $content['name'];
    echo "</h1>";

    echo '<img src="' . $content['image'] . '" />';
    echo '<div>';
    echo $content['text'];
    echo '</div>';
}
